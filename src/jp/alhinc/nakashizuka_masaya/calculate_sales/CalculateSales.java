package jp.alhinc.nakashizuka_masaya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		Map<String, String> branchNames = new HashMap<String, String>();//キーは支店コード、値は支店名
		Map<String, Long> branchSales = new HashMap<String, Long>();//キーは支店コード、値はその支店の合計売上金額
		Map<String, String> commodityNames = new HashMap<String, String>();//キーは商品コード、値は商品名
		Map<String, Long> commoditySales = new HashMap<String, Long>();//キーは商品コード、値はその商品の合計売上金額

		//コマンドライン引数が1つ指定されていなければエラー
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//定義ファイルの読み込み(メソッド呼び出し)
		if(!(definitionRegister("支店", args[0], "branch.lst", "^\\d{3}$", branchNames, branchSales))){
			return;
		}
		if(!(definitionRegister("商品", args[0], "commodity.lst", "^\\w{8}$", commodityNames, commoditySales))) {
			return;
		}

		//売上集計(メソッド呼び出し)
		if(!(calc(args[0], branchSales, commoditySales))) {
			return;
		}

		//集計ファイル出力(メソッド呼び出し)
		if(!(output(args[0], "branch.out", branchNames, branchSales))) {
			return;
		}
		if(!(output(args[0], "commodity.out", commodityNames, commoditySales))) {
			return;
		}

	}


	//定義ファイル読み込みメソッド
	static boolean definitionRegister(String type, String address, String fileName, String regex, Map<String, String> names, Map<String, Long> sales){
		File file = new File(address, fileName);

		//定義ファイルが無ければエラー
		if(!file.exists()) {
			System.out.println(type + "定義ファイルが存在しません");
			return false;
		}

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));

			//支店定義ファイルの中身を読み込み
			String line;
			while((line = br.readLine()) != null) {
				String[] codeAndName = line.split(",");//コードと名前を配列に入れる

				//カンマがない場合と、コードがフォーマット通りでない場合はエラー
				if((codeAndName.length != 2) || (!(codeAndName[0].matches(regex)))) {
					System.out.println(type + "定義ファイルのフォーマットが不正です");
					return false;
				}

				//マップに「コードと名前」、「コードと合計金額」を登録
				names.put(codeAndName[0], codeAndName[1]);
				sales.put(codeAndName[0], 0L);//合計金額の初期値を0で設定

			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	//売上集計メソッド
	static boolean calc(String address, Map<String, Long> branchSales, Map<String, Long> commoditySales) {
		//addressに入ってる全ファイルを一旦取得
		File[] allFiles = new File(address).listFiles();

		//売上ファイルを抽出
		List<File> rcdFiles = new ArrayList<File>();//売上ファイルのみ格納するリスト
		for(int i = 0; i < allFiles.length; i++) {
			if(allFiles[i].isFile() && (allFiles[i].getName().matches("^\\d{8}+.rcd$"))) {
				//ArrayListへFileオブジェクト格納
				rcdFiles.add(allFiles[i]);
			}
		}

		//rcdFilesのファイル名たちが連番になっていなければエラー
		Collections.sort(rcdFiles);//一旦並べる(読み込み順がバラバラの可能性があるので)
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			//ファイル名を取得してint型に変換。前後のファイルの差が1なら連番になっている。
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}

		//売上の集計
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				//売上ファイル内の文字列をリストに格納
				String line;
				List<String> rcdStrs = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					rcdStrs.add(line);
				}

				//売上加算
				//売上ファイルの中身が3行でなければエラー
				if(!(rcdStrs.size() == 3)) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return false;
				}

				String branchCode = rcdStrs.get(0);
				String commodityCode = rcdStrs.get(1);
				String fileSale = rcdStrs.get(2);

				//支店コードが不正ならエラー
				if(!(branchSales.containsKey(branchCode))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return false;
				}

				//商品コードが不正ならエラー
				if(!(commoditySales.containsKey(commodityCode))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return false;
				}

				//売上金額が数字じゃなかったらエラー
				if(!(fileSale.matches("^[0-9]+$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

				//合計金額を一旦計算(マップの中身はまだ更新しない)
				long branchSalesAmount = branchSales.get(branchCode) + Long.valueOf(fileSale);
				long commoditySalesAmount = commoditySales.get(commodityCode) + Long.valueOf(fileSale);

				//合計金額が10桁を超える場合はエラー
				if((branchSalesAmount >= 10000000000L) || (commoditySalesAmount >= 10000000000L)){
					System.out.println("合計金額が10桁を超えました");
					return false;
				}

				//合計金額をマップに反映
				branchSales.replace(branchCode, branchSalesAmount);
				commoditySales.replace(commodityCode, commoditySalesAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}finally{
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		}
		return true;
	}


	//集計ファイル出力メソッド
	static boolean output(String address, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(new File(address, fileName)));

			//書き込み。1店舗(1商品)記入するたびに改行
			for(String Code: names.keySet()) {
				bw.write(Code + "," + names.get(Code) + "," + sales.get(Code));
				bw.newLine();
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
